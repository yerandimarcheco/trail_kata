import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        trials: [],
    },
    mutations: {
        trials(state, trials) {
            state.trials = trials;
        },
    },
    actions: {
        newTrial({commit, state}, trialData) {
            const config = {
                headers: {'Content-Type': `multipart/form-data;`}
            };
            axios.post('http://localhost:8088', trialData, config).then(({data}) => {
                const trials = state.trials;
                const trial = data.data;
                trial.id = trials.length + 1;
                trials.push(trial);

                commit('trials', trials);

                return state.trials;
            });
        },
    },
    getters: {
        getTrials: state => {
            return state.trials
        },
        getPointsByParty: state => (party) => {
            const looserPoints = state.trials.filter( trial => { return !trial.matched && trial.looser.party === party}, party)
                .reduce((accumulator, current) => { return accumulator + current.looser.points; }, 0);

            const winnerPoints = state.trials.filter( trial => { return !trial.matched && trial.winner.party === party}, party)
                .reduce((accumulator, current) => { return accumulator + current.winner.points; }, 0);

            return looserPoints + winnerPoints;
        }
    }
});

export default store;
