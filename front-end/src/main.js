import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false;
import Party from './components/Party.vue';
import RoleButton from './components/RoleButton.vue';
import StartButton from './components/StartButton.vue';
import Resume from './components/Resume.vue';
import TrialsHistorical from './components/TrialsHistorical.vue';

Vue.component('role-button',  RoleButton);
Vue.component('party',  Party);
Vue.component('start-button',  StartButton);
Vue.component('start-button',  StartButton);
Vue.component('resume',  Resume);
Vue.component('trials-historical',  TrialsHistorical);

new Vue({
  render: h => h(App),
  store: require('./vuex/trial').default,
}).$mount('#app')
