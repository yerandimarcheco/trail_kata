#Instructions to set up the project

### How do I get set up? ###

1- Enter to the directory of the project. 

2- Run `docker-compose up --build` in a console (Build and up the docker). This action can take a few minutes to completed.
 
3- Install VUE Cli in case you don't have it. Run `npm install -g @vue/cli`. You can find more information here: https://cli.vuejs.org/guide/installation.html.

4- Enter to front-end directory and execute `npm install` and then  `npm run serve`
