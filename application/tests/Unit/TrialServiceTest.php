<?php
namespace Tests\Unit;

use DataProvider\Kind;
use DataProvider\Notary;
use DataProvider\Validator;
use Domain\TrialService;
use PHPUnit\Framework\TestCase;

require __DIR__."/../../Contracts/Role.php";
require __DIR__."/../../DataProvider/Kind.php";
require __DIR__."/../../DataProvider/Notary.php";
require __DIR__."/../../DataProvider/Validator.php";
require __DIR__."/../../Domain/Contract.php";
require __DIR__."/../../Domain/TrialService.php";

class TrialServiceTest extends TestCase
{
    public function testPartyOneWindTheTrial()
    {
        $partyOne = $this->fakeContract(1, 'K');
        $partyTwo = $this->fakeContract(2, 'N');

        $trialService = new TrialService($partyOne, $partyTwo);

        $trialResult = $trialService->determineResult();

        self::assertEquals(1, $trialResult['winner']->getParty());
        self::assertEquals(2, $trialResult['looser']->getParty());
    }

    public function testPartyTwoWindTheTrial()
    {
        $partyOne = $this->fakeContract(1, 'V');
        $partyTwo = $this->fakeContract(2,'N');
        $trialService = new TrialService($partyOne, $partyTwo);

        $trialResult = $trialService->determineResult();

        self::assertEquals(2, $trialResult['winner']->getParty());
        self::assertEquals(1, $trialResult['looser']->getParty());
    }

    public function testMatchedTrial()
    {
        $partyOne = $this->fakeContract(1, 'N');
        $partyTwo = $this->fakeContract(2, 'N');
        $trialService = new TrialService($partyOne, $partyTwo);

        $trialResult = $trialService->determineResult();

        self::assertTrue( $trialResult['matched']);
    }

    private function fakeContract($party, $role)
    {
        $role = $this->fakeRole($role);
        return new \Domain\Contract($party,'ABCDE', $role);
    }

    private function fakeRole($role)
    {
        $fakeRole = null;
        if ($role == 'K') {
            $fakeRole = new Kind();
        } elseif ($role == 'N') {
            $fakeRole = new Notary();
        } else {
            $fakeRole = new Validator();
        }

        return $fakeRole;
    }
}
