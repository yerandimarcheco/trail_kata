<?php


namespace Domain;


use Contracts\Role;

class Contract
{
    /**
     * @var integer
     */
    private $party;
    /**
     * @var string
    */
    private $identifier;
    /**
     * @var Role
    */
    private $role;

    public function __construct($party, $identifier, $role)
    {
        $this->party = $party;
        $this->identifier = $identifier;
        $this->role = $role;
    }

    /**
     * @return integer
    */
    public function getPoints()
    {
        return $this->role->getPoints();
    }

    /**
     * @return array
    */
    public function getInformation()
    {
        return [
            'party' => $this->party,
            'identifier' => $this->identifier,
            'points' => $this->getPoints()
        ];
    }

    /**
     * @return integer
    */
    public function getParty()
    {
        return $this->party;
    }
}
