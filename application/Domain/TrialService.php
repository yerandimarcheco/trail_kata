<?php
namespace Domain;

class TrialService
{
    /**
     * @var Contract
    */
    private $partyOne;

    /**
     * @var Contract
     */
    private $partyTwo;

    public function __construct(Contract $partyOne, Contract $partyTwo)
    {
        $this->partyOne = $partyOne;
        $this->partyTwo = $partyTwo;
    }

    /**
     * @return array
    */
    public function determineResult()
    {
        $winner = null;
        $looser = null;
        $matched = false;

        if ($this->partyOne->getPoints() > $this->partyTwo->getPoints()) {
            $winner = $this->partyOne;
            $looser = $this->partyTwo;
        } elseif ($this->partyOne->getPoints() < $this->partyTwo->getPoints()) {
            $winner = $this->partyTwo;
            $looser = $this->partyOne;
        } else {
            $matched = true;
        }

        return ['winner' => $winner, 'looser' => $looser, 'matched' => $matched];
    }
}
