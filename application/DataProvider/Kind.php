<?php
namespace DataProvider;

use Contracts\Role;

class Kind implements Role
{
    public function getPoints()
    {
        return 5;
    }
}
