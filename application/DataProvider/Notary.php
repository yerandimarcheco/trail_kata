<?php
namespace DataProvider;

use Contracts\Role;

class Notary implements Role
{
    public function getPoints()
    {
        return 2;
    }
}
