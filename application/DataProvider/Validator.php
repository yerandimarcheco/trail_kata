<?php
namespace DataProvider;

use Contracts\Role;

class Validator implements Role
{
    public function getPoints()
    {
        return 1;
    }
}
