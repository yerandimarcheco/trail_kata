<?php
require(__DIR__ . '/Autoload.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$data = json_decode(file_get_contents("php://input"));

if (!is_null($data)) {
    $rolePartyOne = Helper\Util::roleGenerator($data->partyOne->role);
    $rolePartyTwo = Helper\Util::roleGenerator($data->partyTwo->role);

    $partyOne = new Domain\Contract(1, $data->partyOne->identifier, $rolePartyOne);
    $partyTwo = new Domain\Contract(2, $data->partyTwo->identifier, $rolePartyTwo);

    $trial = new Domain\TrialService($partyOne, $partyTwo);

    $trialResult = $trial->determineResult();

    echo (new \Resources\Trial($trialResult['matched'], $trialResult['winner'], $trialResult['looser']))->toJson();


} else {
    echo json_encode('A winner cannot be determined, there is not enough data');
}
