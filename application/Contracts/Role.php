<?php

namespace Contracts;

interface Role
{
    public function getPoints();
}
