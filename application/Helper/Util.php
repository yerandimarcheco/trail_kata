<?php
namespace Helper;

use DataProvider\Kind;
use DataProvider\Notary;
use DataProvider\Validator;

class Util
{
    /**
     * @param string $role
     * @return Role
     */
    public static function roleGenerator($role)
    {
        $roleObject = null;

        if ($role == 'K') {
            $roleObject = new Kind();
        } elseif ($role == 'N') {
            $roleObject = new Notary();
        } else {
            $roleObject = new Validator();
        }

        return $roleObject;
    }

}
