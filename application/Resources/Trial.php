<?php
namespace Resources;

use Domain\Contract;
use phpDocumentor\Reflection\Types\Boolean;

class Trial
{
    /**
     * @var Contract
    */
    private $winner;
    /**
     * @var Contract
     */
    private $looser;
    /**
     * @var boolean
     */
    private $matched;

    public function __construct($matched, $winner = null, $looser = null)
    {
        $this->winner = $winner;
        $this->looser = $looser;
        $this->matched = $matched;
    }

    public function toJson()
    {
        try {
            $response = new \stdClass();
            $response->winner = (is_null($this->winner)) ? $this->winner : $this->winner->getInformation();
            $response->looser = (is_null($this->looser)) ? $this->looser : $this->looser->getInformation();
            $response->matched = $this->matched;

            return json_encode(['data' => $response]);
        } catch (\ErrorException $errorException) {
            die(var_dump($errorException));
        }
    }
}
